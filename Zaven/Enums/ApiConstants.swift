//
//  ApiConstants.swift
//  Zaven
//
//  Created by Arkadiusz Buraczek on 24/09/2019.
//  Copyright © 2019 Arkadiusz Buraczek. All rights reserved.
//

import Foundation

enum ApiConstants {
    static let baseURL = "https://api.themoviedb.org/3"
    static let token = "d062ea33930fa7da2d56078e444418fe"
}

// MARK: - ENDPOINTS

enum Endpoints {
    static let movies = Movies.self
}

enum Movies {
    private static let base = "/movie"
    static let topRated = base + "/top_rated"
}
