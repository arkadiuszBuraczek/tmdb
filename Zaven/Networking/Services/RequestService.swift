//
//  RequestService.swift
//  Zaven
//
//  Created by Arkadiusz Buraczek on 24/09/2019.
//  Copyright © 2019 Arkadiusz Buraczek. All rights reserved.
//

import Foundation

struct RequestService {
    
    private enum Constants {
        static let timeInterval: Double = 30.0
    }
    
    func getRequest<T: Decodable>(with url: URL, objectType: T.Type, completion: @escaping (ResponseResult<T>) -> Void) {
        let session = URLSession.shared
        let request = URLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: Constants.timeInterval)
        
        session.dataTask(with: request, completionHandler: { data, response, error in
            guard error == nil else {
                guard let err = error else { return }
                completion(ResponseResult.failure(RequestError.networkError(err)))
                return
            }
            
            guard let data = data else {
                completion(ResponseResult.failure(RequestError.dataNotFound))
                return
            }
            
            do {
                let decodedObject = try JSONDecoder().decode(objectType.self, from: data)
                completion(ResponseResult.success(decodedObject))
            } catch let error {
                completion(ResponseResult.failure(RequestError.jsonParsingError(error as! DecodingError)))
            }
        }).resume()
    }
}
