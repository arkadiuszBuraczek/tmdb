//
//  MainMoviesListService.swift
//  Zaven
//
//  Created by Arkadiusz Buraczek on 24/09/2019.
//  Copyright © 2019 Arkadiusz Buraczek. All rights reserved.
//

import Foundation

struct MainMoviesListService {
    private enum Constants {
        static let apiKey = "api_key"
        static let languageKey = "language"
        static let languageValue = "pl-PL"
        static let pageKey = "page"
    }
    
    private let requestService = RequestService()
    
    func fetchMovies(pageNumber: Int, completion: @escaping (ResponseResult<MoviesList>) -> ()) {
        var stringURL = ApiConstants.baseURL + Endpoints.movies.topRated
        
        guard let components = NSURLComponents(string: stringURL) else { return completion(.failure(.urlError)) }
        let queryItems = [URLQueryItem(name: Constants.apiKey, value: ApiConstants.token),
                          URLQueryItem(name: Constants.languageKey, value: Constants.languageValue),
                          URLQueryItem(name: Constants.pageKey, value: "\(pageNumber)")]
        components.queryItems = queryItems
        
        guard let url = components.url else { return completion(.failure(.urlError)) }
        requestService.getRequest(with: url, objectType: MoviesList.self) { result in
              return completion(result)
        }
    }
}
