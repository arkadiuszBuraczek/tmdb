//
//  NetworkErrorHandlers.swift
//  Zaven
//
//  Created by Arkadiusz Buraczek on 24/09/2019.
//  Copyright © 2019 Arkadiusz Buraczek. All rights reserved.
//

import Foundation

enum RequestError: Error {
    case networkError(Error)
    case dataNotFound
    case urlError
    case jsonParsingError(Error)
    case invalidStatusCode(Int)
    case requestFailure
}

enum ResponseResult<T> {
    case success(T)
    case failure(RequestError)
}
