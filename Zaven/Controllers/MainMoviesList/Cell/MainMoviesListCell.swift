//
//  MainMoviesListCell.swift
//  Zaven
//
//  Created by Arkadiusz Buraczek on 24/09/2019.
//  Copyright © 2019 Arkadiusz Buraczek. All rights reserved.
//

import UIKit

final class MainMoviesListCell: UITableViewCell {

    static let identifier = "MainMoviesListCell"
    
    weak var viewModel: MainMoviesListViewModelProtocol? {
        didSet {
            guard let viewModel = viewModel else { return }
            setupData(with: viewModel)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    private func setupData(with: MainMoviesListViewModelProtocol) {
        #warning("setup data")
    }

}
