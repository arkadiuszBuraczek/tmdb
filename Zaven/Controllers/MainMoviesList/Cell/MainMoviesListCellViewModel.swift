//
//  MainMoviesListCellViewModel.swift
//  Zaven
//
//  Created by Arkadiusz Buraczek on 24/09/2019.
//  Copyright © 2019 Arkadiusz Buraczek. All rights reserved.
//

import Foundation

protocol MainMoviesListCellViewModelProtocol: class {
    
}

final class MainMoviesListCellViewModel: MainMoviesListCellViewModelProtocol {
    #warning("init with some renderable")
    init() {
        
    }
}
