//
//  MainMoviesListViewController.swift
//  Zaven
//
//  Created by Arkadiusz Buraczek on 24/09/2019.
//  Copyright © 2019 Arkadiusz Buraczek. All rights reserved.
//

import UIKit

final class MainMoviesListViewController: BaseViewController {

    @IBOutlet private weak var tableView: UITableView!
    

    private let viewModel: MainMoviesListViewModelProtocol
    
    init(viewModel: MainMoviesListViewModelProtocol) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("did open main list")
    }
    
    private func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(MainMoviesListCell.self, forCellReuseIdentifier: MainMoviesListCell.identifier)
    }
}

// MARK: TABLEVIEW DELEGATE
extension MainMoviesListViewController: UITableViewDelegate {
    
}


// MARK: TABLEVIEW DATA SOURCE
extension MainMoviesListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.getNumberOfRowsInSection()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        #warning("todo configure cell")
        
        return UITableViewCell()
    }
}

// MARK: VC DELEGATE
extension MainMoviesListViewController: MainMoviesViewControllerDelegate {
    func showIndicator(_ shouldShow: Bool) {
        shouldShow ? showActivityIndicator() : hideActivityIndicator()
    }
    
    func reloadData() {
        tableView.reloadData()
    }
    
    func showErrorAlert(error: RequestError) {
        showInfoAlert(with: error)
    }
}
