//
//  MainMoviesListViewModel.swift
//  Zaven
//
//  Created by Arkadiusz Buraczek on 24/09/2019.
//  Copyright © 2019 Arkadiusz Buraczek. All rights reserved.
//

import Foundation

protocol MainMoviesListViewModelProtocol: class {
    func getNumberOfRowsInSection() -> Int
}

protocol MainMoviesViewControllerDelegate: class {
    func showIndicator(_ shouldShow: Bool)
    func reloadData()
    func showErrorAlert(error: RequestError)
}

final class MainMoviesListViewModel: MainMoviesListViewModelProtocol {

    private let coordinator: MainMoviesListCoordinatorProtocol!
    private let service = MainMoviesListService()
    
    weak var delegate: MainMoviesViewControllerDelegate! {
        didSet {
            #warning("todo fetch data")
            fetchMovies()
        }
    }
    
    init(coordinator: MainMoviesListCoordinatorProtocol) {
        self.coordinator = coordinator
    }
    
    func getNumberOfRowsInSection() -> Int {
        #warning("return number of items in datasource")
        return .zero
    }
    
    private func fetchMovies() {
        delegate.showIndicator(true)
        #warning("fetch movies")
    }
}
