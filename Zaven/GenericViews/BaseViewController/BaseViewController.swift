//
//  BaseViewController.swift
//  Zaven
//
//  Created by Arkadiusz Buraczek on 24/09/2019.
//  Copyright © 2019 Arkadiusz Buraczek. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    
    private enum Constants {
        static let indicatorSize = CGSize(width: 45, height: 45)
    }
    
    private let activityIndicator = UIActivityIndicatorView(style: .medium)

    override func viewDidLoad() {
        super.viewDidLoad()
        configureActivityIndicator()
    }
    
    func showActivityIndicator() {
        DispatchQueue.main.async { [weak self] in
            self?.activityIndicator.startAnimating()
            self?.view.isUserInteractionEnabled = false
        }
    }
    
    func hideActivityIndicator() {
        DispatchQueue.main.async { [weak self] in
            self?.activityIndicator.stopAnimating()
            self?.view.isUserInteractionEnabled = true
        }
    }
    
    func showInfoAlert(with error: RequestError) {
        var message = ""
        
        switch error {
        case .networkError(_):
            message = NSLocalizedString("error_network_error", comment: "")
            break
        case .invalidStatusCode(_):
            message = NSLocalizedString("error_status_code_wrong", comment: "")
            break
        case .requestFailure:
            message = NSLocalizedString("error_fetch_data", comment: "")
            break
        default:
            message = NSLocalizedString("error_default_message", comment: "")
        }
        
        let alert = UIAlertController(title: NSLocalizedString("alert_title", comment: ""),
                                      message: message ,
                                      preferredStyle: .alert)
        let alertConfirmButton = UIAlertAction(title: NSLocalizedString("alert_confirm_button", comment: ""), style: .default)
        
        alert.addAction(alertConfirmButton)
        
        DispatchQueue.main.async { [weak self] in
            self?.present(alert, animated: true, completion: nil)
        }
    }
    
    private func configureActivityIndicator() {
        view.addSubview(activityIndicator)
        activityIndicator.hidesWhenStopped = true
        
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            activityIndicator.widthAnchor.constraint(equalToConstant: Constants.indicatorSize.width),
            activityIndicator.heightAnchor.constraint(equalToConstant: Constants.indicatorSize.height),
            activityIndicator.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            activityIndicator.centerYAnchor.constraint(equalTo: view.centerYAnchor)
        ])
    }
}
