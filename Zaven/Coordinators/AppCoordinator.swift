//
//  AppCoordinator.swift
//  Zaven
//
//  Created by Arkadiusz Buraczek on 24/09/2019.
//  Copyright © 2019 Arkadiusz Buraczek. All rights reserved.
//

import UIKit

protocol AppCoordinatorProtocol: Coordinator {
    init(window: UIWindow?)
}

final class AppCoordinator: AppCoordinatorProtocol {
    
    private let navigationController = UINavigationController()
    
    private weak var window: UIWindow?
    private var mainMoviesListCoordinator: MainMoviesListCoordinator?
    
    init(window: UIWindow?) {
        self.window = window
        window?.rootViewController = navigationController
    }
    
    func start() {
        showMainMoviesListCoordinator()
    }
    
    private func showMainMoviesListCoordinator() {
        mainMoviesListCoordinator = MainMoviesListCoordinator(coordinator: self, navigationController: navigationController)
        mainMoviesListCoordinator?.start()
    }
}
