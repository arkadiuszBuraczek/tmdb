//
//  MainMoviesListCoordinator.swift
//  Zaven
//
//  Created by Arkadiusz Buraczek on 24/09/2019.
//  Copyright © 2019 Arkadiusz Buraczek. All rights reserved.
//

import UIKit

protocol MainMoviesListCoordinatorProtocol: Coordinator {}

final class MainMoviesListCoordinator: MainMoviesListCoordinatorProtocol {

    private weak var parentCoordinator: Coordinator!
    private weak var navigationController: UINavigationController!
    
    init(coordinator: Coordinator, navigationController: UINavigationController) {
        self.parentCoordinator = coordinator
        self.navigationController = navigationController
    }
    
    func start() {
        let viewModel = MainMoviesListViewModel(coordinator: self)
        let viewController = MainMoviesListViewController(viewModel: viewModel)
        viewModel.delegate = viewController
        
        navigationController.pushViewController(viewController, animated: true)
    }
}
