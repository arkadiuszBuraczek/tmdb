//
//  MoviesList.swift
//  Zaven
//
//  Created by Arkadiusz Buraczek on 24/09/2019.
//  Copyright © 2019 Arkadiusz Buraczek. All rights reserved.
//

import Foundation

struct MoviesList: Codable {
    let page: Int?
    let totalResults: Int?
    let totalPages: Int?
    let results: [Movie]?
    
    private enum CodingKeys: String, CodingKey {
        case page
        case results
        case totalResults = "total_results"
        case totalPages = "total_pages"
    }
}

struct Movie: Codable {
    let posterPath: String?
    let backdropPath: String?
    let genreIds: [Double]?
    let voteCount: Int?
    let overview: String?
    let originalTitle: String?
    let voteAverage: Double?
    let popularity: Double?
    let id: Int?
    let originalLanguage: String?
    let releaseDate: String?
    let video: Bool?
    let title: String?
    let adult: Bool?

    enum CodingKeys: String, CodingKey {

        case posterPath = "poster_path"
        case backdropPath = "backdrop_path"
        case genreIds = "genre_ids"
        case voteCount = "vote_count"
        case originalTitle = "original_title"
        case voteAverage = "vote_average"
        case originalLanguage = "original_language"
        case releaseDate = "release_date"
        case overview
        case popularity
        case id
        case video
        case title
        case adult

    }
}
